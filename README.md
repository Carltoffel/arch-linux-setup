# Arch Linux Setup
This tutorial describes the installation of Arch Linux.

Details:
- UEFI boot with systemd
- Encryption with LUKS
- LVM (on LUKS)

Hint:
- if you need to replace a part inside a command it will be marked with angle brackets (`<yourname>`)
- optional parts are enclosed by square brackets (`[optional]`)
- `{opt1|opt2}` indicates you have to make a decision between `opt1` or `opt2`



## Prepare the Installation Medium
1. Download the [Arch Linux ISO](https://archlinux.org/download/) and save the .iso file
2. Write the .iso on an empty USB-stick (or something comparable) 

    1. Get the name of the installation device
        - `lsblk` (in this guide we use `<sdx>`)
    2. Make sure that it is not mounted
        - No mountpoint with `lsblk`
        - `umount` to unmount
    3. Copy the .iso file onto the device
        - <mark>**data on device will be lost**</mark>
        - `cp <path/to/iso> /dev/<sdx>`
            - `<sdx>` without partition number (root name in `lsblk`)



## Start Installation
1. Plug in the prepared medium and boot the Arch Linux ISO.
    - "Secure Boot" has to be deactivated
2. Make sure the ISO is booted as UEFI
    - Either check the info behind the boot entry in the Arch Linux boot loader screen
    - Or execute `ls /sys/firmware/efi/efivars`, if the directory exists you are in UEFI mode
    - It may be necessary to enable UEFI boot in the BIOS/UEFI
    - If you have no UEFI mode or want to install with BIOS boot, your boot partition and bootloader will differ from this guide

### Connect to Network
The installation needs an internet connection. To check the available adapters run `ip link`. Ethernet adapters are typically called enp... while wireless adapters are called wlp... or wlan0...
#### Ethernet
1. Connect the ethernet with a network

#### Wifi
1. Find out the name of your wifi (`<MYSSID>`)
    - Read the name from your smartphone
    - or call `iwctl` and inside the `iwctl` shell `station list` and `station wlp... scan`
        - (you can directly connect with `iwctl`, see below)
2. There are many ways to connect with a wireless network, choose one:
    - With `iwctl`
        - Call `station wlp... connect <MYSSID> [<password>]`)
    - With `wpa_supplicant` and `wpa_passphrase`
        - Call `wpa_supplicant -B -i <wlp...> -c <(wpa_passphrase <MYSSID> <passphrase>)`
    - By configuring `wpa_supplicant` manually

Tips:
- Check if your connection is working with `ping <web url>`
- adapters may be turned off or soft blocked, check and change status with `rfkill`
- It may be necessary to (re)start `dhcpcd` with your adapter as first argument, to connect properly

### Update System Clock
1. Set the time zone
    - `timedatectl set-timezone <timezone>`
    - `<timezone>` is e.g. "Europe/Berlin" or "Canada/Eastern" like in `/usr/share/zoneinfo`
2. Activate time synchronisation
    - `timedatectl set-ntp true`



## Setup Disk
1. Identify the disk where you want to install Arch Linux
    - <mark>**Data on selected disk will be lost**</mark>
    - `lsblk`, same as above when selecting the installation medium
    - SATA Disks are typically called `sda`, `sdb`, etc. (`sda` will probably be your installation medium)
    - NVME SSDs are typically called `nvme0n1` or similar, partions on it are `nvme0n1p1` etc.
2. Setup the partitions
    - with `gdisk /dev/<sdx>`
        1. `o` to create a GPT (GUID Partition Table)
        2. Create a boot partition of min 512 MB size
            - `n` to create a new partition
            - default partition number and sector start are good choices
            - end sector defines the size, `+512M` creates a 512 MB partition size
            - `ef00` for efi boot partition
        3. Create a main partition for the LVM
            - `n`
            - everything else can be default to select the rest of the disk
            - other partitions are created on the LVM, but you also can define more partitions here if you want
            - `+<x>G` to define offsets (sector start/end) with GB size
        4. Check if the table looks fine
            - `p` to print the table in `gdisk`
        5. Save
            - `w` to write the new partition table


### Encryption and LVM
- This section is optional
- <mark>**Extra caution with LUKS encryption, a damaged header makes the encrypted data unrestoreable. Create backups**</mark>
- In this example, the encrypted partition will be `<disk>`

1. Set up Encryption
    1. Load the encryption module
        - `modprobe dm-crypt`
    2. Encrypt the data partition
        - Select the name of the main partition (`<disk>`)
        - `cryptsetup -c aes-xts-plain -y -s 512 luksFormat /dev/<disk>`
    3. Open the encrypted device
        - Choose a name for the decrypted partition (`<luks>`)
            - Create a mapping with `<luks>` to `<disk>` to access it decrypted
        - `cryptsetup luksOpen /dev/<disk> <luks>`
2. Set up LVM
    1. Create a physical LVM volume on the decrypted partition
        - `pvcreate /dev/mapper/<luks>`
    2. Create a volume group (`<vg>`) in the physical LVM volume (`<luks>`)
        - `vgcreate <vg> /dev/mapper/<luks>`
3. Create the preferred partitions (logical volumes `-n <lv>`) in the volume group, e.g.:
    - Only one partition (partition name is `<root>`) which takes the whole space
        - `lvcreate -l 100%FREE -n <root> <vg>`
    - All in one (`<root>`) but with a swap partition (`<swap>`)
        - The size of the swap partition needs to fit the content of the RAM if you want to suspend to disk. To be on the safe side, if you have the disk space just set it to be the same as your RAM size (in this example 4 GB).
        - `lvcreate -L 4G -n <swap> <vg>`
        - `lvcreate -l 100%FREE -n <root> <vg>`
    - Separate root and home partitions
        - `lvcreate -L 50G -n <root> <vg>`
        - `lvcreate -L 4G -n <swap> <vg>`
        - `lvcreate -l 100%FREE -n <home> <vg>`

### Format partitions and mount them
1. Format the new partitions. Find your LVM partitons in `/dev/mapper/`, the name should be something like `<vg>-<lv>`
    - Format the boot partition with `fat32`
        - `mkfs.fat -F 32 [-n <LABEL>] /dev/<sdx1>`
        - select the partition you created with `gdisk`
    - Format the systems partitions (`root`, `home`, etc.)
        - `mkfs.ext4 [-L <label>]  /dev/mapper/<vg-lv>`
        - You can label your partitions to make them easier to match
    - If you have a swap partition set it up now
        - `mkswap /dev/mapper/<vg-swap>`
2. Mount the partitions
    1. Mount the root partition
        - `mount /dev/mapper/<vg-root> /mnt`
    2. Mount the boot partition
        - create boot directory (`mkdir /mnt/boot/`)
        - `mount /dev/<sdx1> /mnt/boot/`
    3. Mount all other partitions e.g.:
        - `mkdir /mnt/home`
        - `mount /dev/mapper/<vg-home> /mnt/boot`
    4. If you have a swap partition activate it now
        - `swapon /dev/mapper/<vg-swap>`



## Install Base System
0. Optionally uncomment the closest mirror servers in `/etc/pacman.d/mirrorlist` 
1. Install the most important packages onto the mounted root partition
    - `pacstrap /mnt base base-devel linux linux-firmware`
    - If you use LVM install this packages, too
        - `lvm2`
    - Other useful packages:
        - `sudo` root privileges for users
        - `openssh` for ssh connections
        - `zsh` shell alternative
        - `neovim` vim but better
        - `dhcpcd` needed for network connections with dhcp
        - `wpa_supplicant` wireless network connection
        - `intel-ucode` or `amd-ucode` microcode updates for CPU
        - `networkmanager` or `connman` network managers
2. Generate the fstab
    - `genfstab -Lp /mnt > /mnt/etc/fstab`
    - If you prefer UUIDs over Labels replace `L` with `U`
3. Switch to the new system into the so called "chroot jail" environment
    - `arch-chroot /mnt`
    - <mark>**From now on we work on the new system and not any longer on the booted ISO**</mark>
    - After this command with `/` we address the mount point previously known as `/mnt`
4. Configure locale
    1. Uncommend the preferred locales in `/etc/locale.gen`
    2. Set the language (in this case English UTF-8)
        - `localectl set-locale LANG=en_US.UTF-8`
    3. `locale-gen`
5. Set the keymap for the login terminal (e.g. `KEYMAP=de-latin1`)
    - `echo KEYMAP=<keymap> > /etc/vconsole.conf`
6. If you use LVM and/or encryption change the hooks in `mkinitcpio`
    - `nvim /etc/mkinitcpio.conf`
    - Add/Change order of the `HOOKS`:
        - For encryption set `keyboard` and `keymap` before `encrypt`
        - For LVM set `lvm2` before filesystem (and after `encrypt` if you use LVM on LUKS)
7. Change the root password
    - `passwd`



## Setup Boot Loader
There are multiple methods/boot loaders, choose one:
- Systemd Boot
    1. Install the systemd boot loader
        - `bootctl install`
    2. Create boot entry in `/boot/loader/entries/<entry>.conf` (e.g. `arch.conf`)

                title    <title>
                linux    /vmlinuz-linux
                initrd   /{amd|intel}-ucode.img
                initrd   /initramfs-linux.img
                options  cryptdevice=/dev/<encrypted partition>:<luks> root=/dev/mapper/<vg-vlroots> rw

        1. line: The title will be listet in your boot menu
            - e.g. `title Arch Linux`
        2. line: Define the kernel image
            - this should match a file in `/boot/`
        3. line: Define the proper microcode update image
            - Must appear before `initramfs`
            - You can list both, `amd-ucode.img` and `intel-ucode`
            - You have also to install the `amd-ucode` or `intel-ucode` package
        4. line: Define the initramfs image
            - this should match a file in `/boot/`
            - the initramfs contains a subset of the root filesystem and an `init` script to mount the root filesystem
        5. line: Add options (`options <option1> [<option2>] ...`)
            - If you use encryption add `cryptdevice=/dev/<encrypted partition>:<luks>`
            - Add the root partition
                - `root=/dev/mapper/<vg-vlroot>`
            - To make the system writable (default is read only) add `rw`
    3. Create a fallback entry `/boot/loader/entries/<entry-fallback>.conf` (e.g. `arch-fallback.conf`)
        - same as `arch.conf`, but with `initramfs-linux-fallback.img` and you should use another title (e.g `Arch Linux Fallback`)
    4. Create boot loader config in `/boot/loader/loader.conf`

                default <entry>
                editor no
                timeout <seconds>
    
        1. line: Set the default entry which will be selected automatically
            - `default arch`
        2. line: Deactivate the editor for security reasons
        3. line: Set time in seconds until the default entry gets launched
            - `timeout 0` or leaving out this option will immediately launch the default entry, unless space or another key was pressed during boot
- Grub
- Syslinux



## Reboot
1. Exit the chroot jail
    - Ctrg+D or `exit`
    - Now you should be on the ISO system, the new Arch Linux filesystem should be in `/mnt/` again
2. Unmount all previously mounted partitions
    - `umount -R /mnt`
        - `-R` unmounts partitions recursively, without this option you have to do this on your own
    - This step prevents rebooting while write operations are still running
3. Reboot the system
    - `reboot`
    - Make sure the motherboard boots the correct disk (and not the ISO, unplug it)
    - If Arch Linux is not booting:
        1. Reboot the ISO
        2. Mount all partitions as you did before
            - If you use encryption use `cryptsetup luksOpen /dev/<partition> <luks>` to decrypt the partition
        3. Change into the new system (`arch-chroot /mnt`)
        4. Check/repeat the installation of the boot loader
            - All files spelled correctly?
            - Content of boot loader configs correct?
            - `vmlinuz-linux` and `initramfs-linux.img` in `/boot/` present?
                - The installation of the `linux` package places them there, if you mounted the boot partition correctly



## Add Users and Groups



## Display Manager and Desktop Environment



## Recommendations

### Packages
